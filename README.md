# xiact

## Dependencies

```
pip install -r requirements.txt 
```

or

```
source spectrocrunch
```

## Concatenate xiact data in HDF5

```
python concatenate.py /data/visitor/ls3280/id16b/20240125/RAW_DATA/061_S05_corl_diff_fluo_TS_3min_1s_10um_15pts
```

or 

```
./run.sh
```
