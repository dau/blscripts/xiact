#!/bin/bash

SCRIPT_ROOT="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

ROOT_DIR="/data/visitor/ls3280/id16b/20240125"

RAW_DATA="$ROOT_DIR/RAW_DATA/"
RESULT_DIR="$ROOT_DIR/PROCESSED_DATA/wdn"

rm -rf $RESULT_DIR
mkdir $RESULT_DIR

(cd $RESULT_DIR; python $SCRIPT_ROOT/concatenate.py "$ROOT_DIR/RAW_DATA")
