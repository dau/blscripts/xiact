import os
import re
from glob import glob

import fabio
import numpy
import h5py
import pandas
from datetime import datetime
from silx.io.dictdump import dicttonx
from silx.io.h5py_utils import top_level_names


_XIACT_FORMAT = re.compile(
    "^(?P<radix>.+)_(?P<label>xiact)_(?P<run_number>[0-9]+)_0000_0000.edf$"
)


def timestamp():
    return datetime.now().astimezone().isoformat()


class EdfUrl:
    def __init__(self, filename, idx=tuple()):
        self._filename = filename
        self._idx = idx

    def read(self):
        with fabio.open(self._filename) as edf:
            return edf.data[self._idx]


def parse_xiact(xiact_filename):
    parsed = _XIACT_FORMAT.match(xiact_filename).groupdict()
    run_number = int(parsed["run_number"])
    basename = os.path.basename(parsed["radix"])
    lima_filename = f"{parsed['radix']}_{parsed['run_number']}.edf"

    if os.path.exists(lima_filename):
        with fabio.open(lima_filename) as edf:
            header = edf.header
            motor_mne = header["motor_mne"].split(" ")
            motor_pos = list(map(float, header["motor_pos"].split(" ")))
            positioners = dict(zip(motor_mne, motor_pos))
            positioners["energy"] = float(header["energy"])
            count_time = float(header["count_time"])
            detectors = {
                "frelon": {
                    "type": "lima",
                    "image": EdfUrl(lima_filename),
                    "@NX_class": "NXdetector",
                    ">data": "./image",
                }
            }
    else:
        count_time = numpy.nan
        detectors = dict()
        positioners = dict()

    with fabio.open(xiact_filename) as edf:
        header = edf.header
        detnrs = list(map(int, header["xdet"].split(" ")))

        for i, detnr in enumerate(detnrs):
            key = f"xia{detnr:02d}"

            xevt = int(header[f"xevt{detnr:02d}"])
            xicr = int(header[f"xicr{detnr:02d}"])
            xocr = int(header[f"xocr{detnr:02d}"])
            xlt = int(header[f"xlt{detnr:02d}"])

            elapsed_time = xevt / xocr if xocr != 0 else count_time

            detectors[key] = {
                "type": "mca",
                "spectrum": EdfUrl(xiact_filename, idx=i),
                "events": xevt,
                "trigger_count_rate": xicr,
                "event_count_rate": xocr,
                "triggers": xicr * xlt,
                "fractional_dead_time": 1 - float(xocr) / xicr if xicr != 0 else 0.0,
                "trigger_live_time": xlt,
                "live_time": xevt / xicr if xicr != 0 else 0.0,
                "elapsed_time": elapsed_time,
                "live_time@units": "s",
                "elapsed_time@units": "s",
                ">data": "./spectrum",
            }

            if numpy.isnan(count_time):
                count_time = elapsed_time

    detectors["run_number"] = {"data": run_number}
    detectors["elapsed_time"] = {
        "data": count_time,
        "@NX_class": "NXpositioner",
        "data@units": "s",
    }

    return basename, run_number, detectors, positioners


def save_as_loopscan(name, detectors, positioners):
    filename = f"{name}.h5"

    if os.path.exists(filename):
        names = top_level_names(filename)
        scannr = int(max(map(float, names))) + 1
    else:
        with h5py.File(filename, mode="a", track_order=True) as nxroot:
            nxroot.attrs["NX_class"] = "NXroot"
            nxroot.attrs["file_time"] = timestamp()
        scannr = 1

    print(f"Save {os.path.abspath(filename)}::/{scannr}.1")

    detectors = merge_detector_data(detectors)

    elapsed_time = detectors["elapsed_time"]["data"]
    title = f"loopscan {len(elapsed_time)} {elapsed_time[0]}"

    instrument = {
        "@NX_class": "NXinstrument",
        "positioners": {"@NX_class": "NXcollection", **positioners},
    }
    instrument.update(detectors)

    entry = {
        "@NX_class": "NXentry",
        "title": title,
        "instrument": instrument,
    }

    with h5py.File(filename, mode="a", track_order=True) as nxroot:
        key = f"{scannr}.1"
        nxentry = nxroot["/"].create_group(key, track_order=True)
        nxentry["start_time"] = timestamp()
        dicttonx(entry, nxentry)
        nxentry["end_time"] = timestamp()


def merge_detector_data(scans):
    scan0_detectors = scans[0]

    # String fields are the same for all ct's
    data = {
        name: {
            field: value
            for field, value in scan0_detectors[name].items()
            if isinstance(value, str)
        }
        for name in scan0_detectors
    }

    # Concatenate numeric fields of all ct's
    for name in scan0_detectors:
        for field, value in scan0_detectors[name].items():
            if isinstance(value, str):
                continue
            data[name][field] = [
                _dereference_value(scan_detectors[name][field])
                for scan_detectors in scans
            ]

    return data


def _dereference_value(value):
    if isinstance(value, EdfUrl):
        return value.read()
    return value


def main(root_dir):
    scan_detectors = list()
    scan_positioners = list()
    xiact_files = glob(os.path.join(root_dir, "**", "*xiact*.edf"), recursive=True)
    print(f"Parsing {len(xiact_files)} xiact files ...")

    for xiact_filename in xiact_files:
        basename, run_number, detectors, positioners = parse_xiact(xiact_filename)

        positioners["name"] = basename
        positioners["run_number"] = run_number

        scan_detectors.append(detectors)
        scan_positioners.append(positioners)

    metadata = pandas.DataFrame.from_dict(scan_positioners, orient="columns")

    special_columns = ("run_number", "name")
    positioner_names = [name for name in metadata if name not in special_columns]
    group_names = ["name"] + positioner_names

    for group_values, group in metadata.groupby(group_names):
        name, *positioner_values = group_values
        positioners = dict(zip(positioner_names, positioner_values))
        group.sort_values(by=["run_number"], inplace=True)
        detectors = [scan_detectors[i] for i in group.index]
        save_as_loopscan(name, detectors, positioners)


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description="Concatenate xiact data")
    parser.add_argument(
        "root_dir", type=str, help="Recursive search for xiact data in this directory"
    )

    args = parser.parse_args()
    main(args.root_dir)
